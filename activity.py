# 1. Create an activity folder and an activity.py inside of it.

# 2. Mimic the given terminal output by creating the following "list".

	#Activity No. 1 
	# a. Create a list named "numbers" that will contain five(5) integer values.
list1 = [1, 2, 3, 4, 5]
print(f"list of numbers: {list1}")

	# b. Create a list named "meals" that will contain three(3) string values.
list2 = ["spam", "Spam", "SPAM!"]
print(f"list of meals: {list2}")

# Activity No. 3
	# 3. Using the common "list literals and operators", mimic the given terminal output.

		# a. Create a "messages" list and insert a string value "Hi!" four times using the repeat operator.
list3 = ["Hi!"]*4
print(f"Result of repeat operator: {list3}")

	# b. Perform the following operations using the "numbers" list.

		# b.1. Print the length of the "numbers" list
print(f"length of the list: {len(list1)}")

		# b.2. Removed the last item on the "numbers" list.
del(list1[-1])
print(f"The last item was removed: 5")

		# b.3. Reverse the sorting order of the "numbers" list items.
list1.reverse()
print(f"List in reverse order: {list1}")

	# c. Perform the following operations using the "meals" list.

		# c.1. Using the indexing operation, print the item "SPAM!" from the "meals" list.
print(f"Using the indexing operation: {list2[2]}")


		# c.2. Using the negative indexing operation, print the item "Spam" from the "L" list.
print(f"Using the negative indexing operation: {list2[-1]}")

		# c.3. Reassign the item "Spam" with "eggs" and print the modified "meals" list.
list2[1] = "eees"
print(f"modified list: {list2}")

		# c.4. Add another list item with a value of "bacon" and print the modified "meals" list.
list2.append("bacon")
print(f"add new item to list: {list2}")

		# c.5. Arrange your list items using the sort method and print the modified "meals" list.
list2.sort()
print(f"modified list after sort method: {list2}")

	# Activity No. 4 (Modified this one)
	# 4. Create a dictionary named "recipe" with an initial key of "eggs" and a value of 3.

print("Created dictionary named recipe:")
recipe = {"eees" : 3}
print(f"recipe = {recipe}")

		# Activity No. 5
		# a. Add the following keys and values in the recipe: 
			# "spam":2, "ham":1, "brunch": bacon
		# and print the "recipe" to view changes. 

recipe["spam"] = 2
recipe["ham"] = 1
recipe["brunch"] = "bacon"
print("modified recipe dictionary:")

print(f"recipe = {recipe}")
		# b. Update the "ham" key value with the following:
			# ["grill", "bake", "fry"]
		# and print the "recipe" to view changes. 
		# c. Delete the "eggs" key in the dictionary and print the "recipe" to view changes.
recipe["ham"] = ["grill", "bake", "fry"]
print("updated ham key value:")

print(f"recipe = {recipe}")

	# Activity No.6
	# 5. Using the provided "bob" dictionary, access and print the following key values in the terminal.

bob = 	{'name': {'first': 'Bob', 'last': 'Smith'},
		 'age': 42,
		 'job': ['software', 'writing'],
		 'pay': (40000, 50000)}
print("given bob dictionary:")

print(f"bob = {bob}")

		# a. Print the "name" values.
print(f"accessing the name key : {bob['name']}")

		# b. Print the "Smith" value in the "name" key.
print(f"accessing the last key : {bob['name']['last']}")

		# c. Print the "5000" value in the "pay" key.
print(f"accessing second value from the pay key : {bob['pay'][1]}")


# 6. Using the provided "numeric" tuple, access and print the following items in the terminal.
numeric= ('twelve', 3.0, [11, 22, 33])
print("given numeric tuple:")

print(f"numeric = {numeric}")
# a. Print the item equal to "3.0"
print(f"accessing second value from the pay key : {numeric[1]}")
# b. Print the item equal to "22"
print(f"accessing second value from the pay key : {numeric[2][1]}")